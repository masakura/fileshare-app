FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY FileShare/*.csproj ./FileShare/
RUN dotnet restore

# copy everything else and build app
COPY FileShare/. ./FileShare/
WORKDIR /app/FileShare
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/FileShare/out ./
ENTRYPOINT ["dotnet", "FileShare.dll", "--urls=http://*:5000"]
EXPOSE 5000
