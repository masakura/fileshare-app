﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace FileShare.Models
{
    public sealed class UploadingFile
    {
        private readonly IFormFile _upload;

        public UploadingFile(IFormFile upload)
        {
            _upload = upload;
        }

        private string FileName => Path.GetFileName(_upload.FileName);

        public void Save(string directory)
        {
            var path = Path.Combine(directory, FileName);

            using var file = new FileStream(path, FileMode.Create);

            _upload.CopyTo(file);
        }
    }
}