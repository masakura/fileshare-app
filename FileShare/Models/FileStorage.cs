﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileShare.Models
{
    public sealed class FileStorage
    {
        private readonly DirectoryInfo _base;

        public FileStorage(string baseDirectory)
        {
            if (!Directory.Exists(baseDirectory)) Directory.CreateDirectory(baseDirectory);

            _base = new DirectoryInfo(baseDirectory);
        }

        public static FileStorage Instance { get; } = Create();

        private static FileStorage Development()
        {
            return new FileStorage(Path.Combine(Environment.CurrentDirectory, "files"));
        }

        private static FileStorage Azure()
        {
            return new FileStorage("/home/site/files");
        }

        private static FileStorage Create()
        {
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
                return Development();

            return Azure();
        }

        public IEnumerable<UploadedFile> Files()
        {
            return _base.EnumerateFiles().Select(file => new UploadedFile(file));
        }

        public void Save(UploadingFile file)
        {
            file.Save(_base.ToString());
        }

        public UploadedFile Find(string id)
        {
            return Files().FirstOrDefault(file => file.Is(id));
        }
    }
}