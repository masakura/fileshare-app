﻿using System;
using System.IO;
using System.Text;

namespace FileShare.Models
{
    public sealed class UploadedFile
    {
        private readonly FileInfo _file;

        public UploadedFile(FileInfo file)
        {
            _file = file;
        }

        public string FileName => _file.Name;

        public string Id => Convert.ToBase64String(Encoding.UTF8.GetBytes(FileName));

        public bool Is(string id)
        {
            return Id.Equals(id);
        }

        public Stream GetStream()
        {
            return _file.OpenRead();
        }
    }
}