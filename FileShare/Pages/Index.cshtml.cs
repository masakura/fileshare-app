﻿using FileShare.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FileShare.Pages
{
    public class IndexModel : PageModel
    {
        public FileStorage Storage { get; } = FileStorage.Instance;

        [BindProperty] public IFormFile Upload { get; set; }

        public IActionResult OnGet(string file)
        {
            if (string.IsNullOrEmpty(file)) return null;

            return File(Storage.Find(file));
        }

        private FileStreamResult File(UploadedFile file)
        {
            return File(file.GetStream(), "application/octet-stream", file.FileName);
        }

        public IActionResult OnPost()
        {
            Storage.Save(new UploadingFile(Upload));

            return RedirectToPage();
        }
    }
}